<?php

/**

 * Sober functions and definitions.

 *

 * @link    https://developer.wordpress.org/themes/basics/theme-functions/

 *

 * @package Sober Child

 */



add_action( 'wp_enqueue_scripts', 'sober_child_enqueue_scripts', 20 );



function sober_child_enqueue_scripts() {

	wp_enqueue_style( 'sober-child', get_stylesheet_uri() );

}



add_action('admin_head', 'my_custom_fonts');



function my_custom_fonts() {

echo '<style>

.toplevel_page_online_designer img {

    opacity: 1 !important;

}

</style>';

}


function my_custom_cart_button_text( $text, $product ) {
    if( $product->is_type( 'variable' ) ){
        $text = __('Buy Now', 'woocommerce');
    }
    return $text;
}
add_filter( 'woocommerce_product_single_add_to_cart_text', 'my_custom_cart_button_text', 10, 2 );