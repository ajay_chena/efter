<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.2
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$post_thumbnail_id = $product->get_image_id();
$wrapper_classes   = apply_filters( 'woocommerce_single_product_image_gallery_classes', array(
	'woocommerce-product-gallery',
	'woocommerce-product-gallery--' . ( has_post_thumbnail() ? 'with-images' : 'without-images' ),
	'woocommerce-product-gallery--columns-' . absint( $columns ),
	'images',
) );
?>
<div class="<?php echo esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) ); ?>" data-columns="<?php echo esc_attr( $columns ); ?>" style="opacity: 0; transition: opacity .25s ease-in-out;">
	<figure class="woocommerce-product-gallery__wrapper">
		<?php
		if ( has_post_thumbnail() ) {
			$html  = wc_get_gallery_image_html( $post_thumbnail_id, true );
		} else {
			$html  = '<div class="woocommerce-product-gallery__image--placeholder">';
			$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src() ), esc_html__( 'Awaiting product image', 'woocommerce' ) );
			$html .= '</div>';
		}

		echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $post_thumbnail_id );

		do_action( 'woocommerce_product_thumbnails' );
		?>
        <div class="product-output">
    		<div class="tit">Scale 1:1 Of <span class="name"></span></div>
            <div class="botot">
          	<span class="length-out">10 CENTIMETRES</span>/
            <span class="weight-out">000</span> Gram/
            <span class="date-out"> 1-01-2018</span>/
            <span class="time-out"></span>
            </div>
    </div>
	</figure>
    
<script id="jsbin-javascript">
jQuery(function(){
  jQuery('.childname').keydown(function(){
    setTimeout(function() {
      jQuery('.name').text(jQuery('.childname').val());
    }, 50);
  });
});
</script>
<script id="jsbin-javascript">
jQuery(function(){
  jQuery('.weight').keydown(function(){
    setTimeout(function() {
      jQuery('.weight-out').text(jQuery('.weight').val());
    }, 50);
  });
});
</script>
<script id="jsbin-javascript">
jQuery(function(){
  jQuery('.date').change(function(){
    setTimeout(function() {
      jQuery('.date-out').text(jQuery('.date').val());
    }, 50);
  });
});
</script>
<script id="jsbin-javascript">
jQuery(function(){
  jQuery('.time').keydown(function(){
    setTimeout(function() {
      jQuery('.time-out').text(jQuery('.time').val());
    }, 50);
  });
});
</script>

<script id="jsbin-javascript">
jQuery(function(){
  jQuery('.length').change(function(){
    setTimeout(function() {
      jQuery('.length-out').text(jQuery('.length').val());
    }, 50);
  });
});
</script>
</div>
